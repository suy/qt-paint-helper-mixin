TEMPLATE = subdirs

qtHaveModule(gui) {
    SUBDIRS += gui
}
qtHaveModule(widgets) {
    SUBDIRS += widgets
}
qtHaveModule(quick) {
    SUBDIRS += quick
}
