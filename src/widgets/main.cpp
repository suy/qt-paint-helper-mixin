#include <QApplication>
#include <QPainter>
#include <QWidget>

#include "painthelper.h"

class Window : public QWidget, public PaintHelper<Window, QPainter>
{
    Q_OBJECT
public:
    explicit Window(QWidget* parent = 0)
        : QWidget(parent)
    {}

    void paintEvent(QPaintEvent*) override
    {
        QPainter painter(this);
        paint(&painter);
    }
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Window window;
    window.setWindowTitle("Painting done on a QWidget");
    window.resize(800, 600);
    window.show();

    return app.exec();
}

#include "main.moc"
