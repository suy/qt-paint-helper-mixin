#include <QGuiApplication>
#include <QPainter>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickPaintedItem>

#include "painthelper.h"

class PaintedItem : public QQuickPaintedItem, public PaintHelper<PaintedItem, QPainter>
{
    Q_OBJECT
public:
    explicit PaintedItem(QQuickItem *parent = 0)
        : QQuickPaintedItem(parent)
    {}

    void paint(QPainter* painter) override
    {
        PaintHelper::paint(painter);
    }
};

// Trick to have in a readable form the source of a QML file inline. Seen at
// Jocelyn Turcotte's presentation on Qt World Summit 2015. Notice the need to
// have semicolons.
static const char* qmlCode = QT_STRINGIFY(
    import QtQuick 2.6;
    import QtQuick.Window 2.2;
    import example 1.0;

    Window {
        visible: true; width: 800; height: 600;
        title: qsTr("Painting performed on a QQuickPaintedItem");

        PaintedItem { anchors.fill: parent }
    }
);

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<PaintedItem>("example", 1, 0, "PaintedItem");
    QQmlApplicationEngine engine;
    engine.loadData(qmlCode);

    return app.exec();
}

#include "main.moc"
