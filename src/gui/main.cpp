#include <QBackingStore>
#include <QGuiApplication>
#include <QPainter>
#include <QRasterWindow>

#include "painthelper.h"

class Window : public QRasterWindow, public PaintHelper<Window, QPainter>
{
    Q_OBJECT
public:
    explicit Window(QWindow *parent = 0)
        : QRasterWindow(parent)
    {}

protected:
    void paintEvent(QPaintEvent*) override
    {
        QPainter painter(this);
        painter.fillRect(0, 0, width(), height(), Qt::white);
        paint(&painter);
    }
};

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    Window window;
    window.setTitle("Rendered performed in a QRasterWindow (no widgets)");
    window.resize(800, 600);
    window.show();

    return app.exec();
}

#include "main.moc"
