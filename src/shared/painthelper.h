template<typename View, typename Painter>
class PaintHelper
{
protected:
    void paint(Painter* painter)
    {
        View* that = static_cast<View*>(this);
        painter->drawText(QRectF(0, 0, that->width(), that->height()), Qt::AlignCenter,
                QStringLiteral("Lorem ipsum"));
    }
};
