#include <QtTest>

#include <QPainter>

#include "painthelper.h"

class tst_Widgets : public QObject
{
    Q_OBJECT

private slots:
    void testWidgets();
};

class MockPainter
{
public:
    void drawText(const QRectF& rectangle, int flags, const QString& text,
                  QRectF* boundingRect = nullptr)
    {
        this->text = text;
        this->rectangle = rectangle;
        QPainter painter(widget);
        painter.drawText(rectangle, flags, text, boundingRect);
    }

    QRectF rectangle;
    QString text;
    QWidget* widget;
};

class MockWindow : public QWidget, public PaintHelper<MockWindow, MockPainter>
{
public:
    MockPainter painter;
    void paintEvent(QPaintEvent*) override
    {
        painter.widget = this;
        paint(&painter);
    }
};

void tst_Widgets::testWidgets()
{
    MockWindow window;
    window.resize(800, 600);
    window.show();

    QTest::qWaitForWindowExposed(&window);
    QCOMPARE(window.painter.text, QString("Lorem ipsum"));
    QCOMPARE(window.painter.rectangle.width(), 800.0);
    QCOMPARE(window.painter.rectangle.height(), 600.0);
}

QTEST_MAIN(tst_Widgets)

#include "tst_widgets.moc"
