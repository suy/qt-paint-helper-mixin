#include <QtTest>

#include "painthelper.h"

class tst_PaintHelper : public QObject
{
    Q_OBJECT

private slots:
    void testPaintHelper();
};

// This has to implement each function that the helper calls on the
// "View" class (so QWidget, QQuickPaintedItem or QRasterWindow).
struct MockView
{
    QSize size;
    int width()  const { return size.width(); }
    int height() const { return size.height(); }
};

class MockPainter
{
public:
    void drawText(const QRectF& rectangle, int flags, const QString& text,
                  QRectF* boundingRect = nullptr)
    {
        Q_UNUSED(flags); Q_UNUSED(boundingRect);
        this->text = text;
        this->rectangle = rectangle;
    }

    QRectF rectangle;
    QString text;
};


class MockGui : public MockView, public PaintHelper<MockGui, MockPainter>
{
public:
    MockPainter painter;
    void paint() { PaintHelper::paint(&painter); }
};

void tst_PaintHelper::testPaintHelper()
{
    MockGui gui;
    gui.size = QSize(800, 600);
    gui.paint();
    QCOMPARE(gui.painter.text, QString("Lorem ipsum"));
    QCOMPARE(gui.painter.rectangle.width(), 800.0);
    QCOMPARE(gui.painter.rectangle.height(), 600.0);
}

QTEST_MAIN(tst_PaintHelper)

#include "tst_painthelper.moc"
